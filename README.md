# Arduino Neopixel UFO

See [the project page on retrojdm.com](http://www.retrojdm.com/ArduinoNeoPixelUFO.asp)

![Arduino Neopixel UFO](./ufo.jpg)

I've just moved to Canada, and they take Halloween very seriously here! I decided to make a giant (2 meter diameter) UFO using cardboard, tinfoil, an Arduino Micro and 72 NeoPixels.

The design was made in Blender. Once I was happy with it I printed templates for each section, and got busy cutting the shapes out of cardboard by hand with a box cutter (the cardboard was liberated from the local cardboard recycling bin).

## Arduino code

You'll need both [Adafruit's NeoPixel library](https://github.com/adafruit/Adafruit_NeoPixel) (see also their [Uberguide](https://learn.adafruit.com/adafruit-neopixel-uberguide/arduino-library)), and my own [NeoPixelAnim](https://gitlab.com/retrojdm/neopixelanim) libary. The NeoPixelAnim example UFO sketch is the one used for this project.

## Cardboard templates

See the [Plans](./Plans) folder for the templates I used. They include guide marks to join multiple page, assuming you're printing in A4.

I've included PDFs, along with the source Photoshop PSDs.
